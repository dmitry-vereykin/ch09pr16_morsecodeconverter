/**
 * Created by Dmitry Vereykin on 7/22/2015.
 */
import java.util.Scanner;

public class MorseCodeConverter {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Enter a string: ");
        String inputRaw = keyboard.nextLine();
        String output = "";

        char[] english = {' ', 'A','B','C','D','E','F','G','H','I','J','K',
                'L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
                '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ',', '.', '?'};

        String[] morse = {" ", ".-", "-...", "-.-.", "-..",  ".", "..-.",
                        "--.", "....", "..", ".---", "-.-", ".-..", "--",
                        "-.", "---", ".--.", "--.-", ".-.", "...", "-", "..-",
                        "...-", ".--", "-..-", "-.--",  "--..", "-----", ".----",
                        "..---", "...--", "....-", ".....", "-....", "--...",
                        "---..", "----.", "--..--", ".-.-.-", "..--.."};

        //I hope there are no typos in arrays

        String input = inputRaw.toUpperCase();
        System.out.print("\nEnglish: " + input);

        char[] inputArr = input.toCharArray();

        for (int n = 0; n < inputArr.length; n++) {
            for (int index = 0; index < english.length; index++) {
                if (inputArr[n] == english[index])
                    output += morse[index];
            }
        }

        keyboard.close();
        System.out.println("\nMorse code: " + output);
    }
}
